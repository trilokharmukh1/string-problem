// this function is convert in title case

function convertTitleCase(obj){
    let fullName = ""
    for(key in obj){
        let objValue = obj[key];
        objValue = objValue.charAt(0).toUpperCase() + objValue.substring(1).toLowerCase()   
        fullName = fullName + " " + objValue;
    }
   return fullName;
}

module.exports = convertTitleCase;