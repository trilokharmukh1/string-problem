//-- convert ip address into array

function ipToArray(ipAddress) {
    if (ipAddress.length < 7 || ipAddress.length > 15) {    //ipv4 length must be greater than 7 and less than 15
        return []
    }

    let ipArray = ipAddress.split(".");

    if (ipArray.length === 4) {

        for (let i = 0; i < ipArray.length; i++) {
            ipArray[i] = Number(ipArray[i]);        // convert string to number

            if (ipArray[i] < 0 || ipArray[i] > 255 || Number.isInteger(ipArray[i]) === false) {
                return []
                // if number not range of ipv4 or contain any chacter or symble than return
            }
        }

        return ipArray;

    } else {
        return []
    }


}

module.exports = ipToArray;