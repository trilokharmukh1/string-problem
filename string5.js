// array to string converter

function arrayToString(array){
    string = array.toString();             // return string saparate by comma

    string = string.replace(/,/g," ");     // all comma replace by space

    return string
}

module.exports = arrayToString;