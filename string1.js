//---convert the given strings into their equivalent numeric format--

function stringToNumeric(string) {
    
    if (string.charAt(0) == '$') {          
         string = string.substring(1);      //remove $ if it is in 0th index

    } else if (string.charAt(0) == '-' && string.charAt(1) == '$') {
        string = string.substring(0, 1) + string.substring(2);
    }

    string = string.replace(/,/g, "")     // replace all ',' with ""

    numericValue = Number(string);
    // convert into number. If string has any alphabatic char or any special charter it will be return NaN

    if(isNaN(numericValue)){
        return 0;
    }

    return numericValue;
}

module.exports = stringToNumeric;